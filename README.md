# BDMM vs TreeTime

Code and additional figures for the Lab Report comparing BDMM and TreeTime's accuracy in inferring SARS-CoV-2 migration events between Switzerland and Europe.