const credentials = require('./secret');
const sql = require('pg');
const _ = require('lodash');

const connection = new sql.Client({
    host: credentials.host,
    port: credentials.port,
    user: credentials.username,
    password: credentials.password,
    database: 'sars_cov_2'
});

async function whereKeyInArray(strTableName, strKey, arrSequenceIds){
    let strQueryString = `SELECT * FROM ${strTableName} WHERE ${strKey} IN (${"'" + _.join(arrSequenceIds, "','") + "'"});`
    let queryResult = null;
    try {
        queryResult = await connection.query(strQueryString);
    } catch (error) {
        console.log(error);
    } finally {
        return queryResult;
    }
}

async function whereKeyInArrayAndDateBetween(strTableName, strKey, arrSequenceIds, strDateColumn, strStartDate, strEndDate){
    let strQueryString = `SELECT * FROM ${strTableName} WHERE ${strKey} IN (${"'" + _.join(arrSequenceIds, "','") + "'"}) AND ${strDateColumn} BETWEEN '${strStartDate}' AND '${strEndDate}';`
    let queryResult = null;
    try {
        queryResult = await connection.query(strQueryString);
    } catch (error) {
        console.log(error);
    } finally {
        return queryResult;
    }
}

async function getIdsByLocation(strTableName, strKeyName, strLocation, strLocationColumnName){
    let strQueryString = `SELECT ${strKeyName} FROM ${strTableName} WHERE ${strLocationColumnName} = '${strLocation}';`
    let queryResult = null;
    try {
        queryResult = await connection.query(strQueryString);
    } catch (error) {
        console.log(error);
    } finally {
        return queryResult;
    }
}

async function connect(){
    return connection.connect();    
}

async function disconnect(){
    return connection.end();  
}

module.exports = {
    whereKeyInArray,
    whereKeyInArrayAndDateBetween,
    getIdsByLocation,
    connect,
    disconnect
}