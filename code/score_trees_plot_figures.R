library(ggplot2)

WORKDIR <- "E:\\CBB\\Clean Lab Rotation\\Data"
setwd(WORKDIR)
RUNS_FOLDER_NAMES <- c("set_1", "set_3", "set_4", "set_5")
# RUNS_FOLDER_NAMES <- c("set_4")
TREE_FOLDER_NAME <- "Trees"
BEAST_TREES = c("fix_summary.tree")
TREETIME_TREES = c("annotated_tree.nexus")
NEWICK_CONFIDENCE = "confidence.csv"
META_FOLDER_NAME = "Generated dataset"
META_FILE = "annotation_file.tsv"
NBINS <- 10
COUNT_THRESHOLD <- 2

build_beast_tree_summary <- function(tree, metadata){
  tree_summary <- list()
  branch_lengths <- c()
  for (i in 1:nrow(metadata)) {
    known_sequence <- metadata[i,] 
    parent <- tree$parent[tree$label == metadata[i,1]]
    inferred_type <- tree$type[tree$node == parent]
    type_probability <- tree$type.prob[tree$label == parent]
    ground_truth <- known_sequence$infected_in_che_or_eur
    type_probability <- tree$type.prob[tree$label == parent]
    branch_length <- tree$branch.length[tree$label == metadata[i,1]]
    branch_lengths <- c(branch_lengths, branch_length)
    sequence_name <- known_sequence[[1]]
    info_summary <- list("name"= sequence_name, "ground_truth" = ground_truth, "inferred_type" = inferred_type[[1]], "prob" = type_probability[[1]], "branch_length" = branch_length)
    tree_summary[[sequence_name]] <- info_summary
  }
  
  return(list("branch_lengths"= branch_lengths, "tree_summary"=tree_summary))
}

build_treetime_tree_summary <- function(tree, confidence, metadata){
  tree_summary <- list()
  branch_lengths <- c()
  
  for (i in 1:nrow(metadata)) {
    known_sequence <- metadata[i,] 
    parent <- tree$parent[tree$label == metadata[i,1] & !is.na(tree$label)]
    parent_label <- tree$label[tree$node == parent & !is.na(tree$node)]
    inferred_type <- tree$location[tree$node == parent & !is.na(tree$node)]
    type_probability <- max(confidence$A[confidence$X.name == parent_label], confidence$B[confidence$X.name == parent_label])
    ground_truth <- known_sequence$infected_in_che_or_eur
    branch_length <- tree$branch.length[tree$label == metadata[i,1] & !is.na(tree$label)]
    branch_lengths <- c(branch_lengths, branch_length)
    sequence_name <- known_sequence[[1]]
    info_summary <- list("name"= sequence_name, "ground_truth" = ground_truth, "inferred_type" = inferred_type[[1]], "prob" = type_probability[[1]], "branch_length" = branch_length)
    tree_summary[[sequence_name]] <- info_summary
  }

  return(list("branch_lengths"= branch_lengths, "tree_summary"=tree_summary))
}


build_trees_summaries <- function(treeFolderName, metadata_file){
  summaries = list();
  
  for(tree_name in BEAST_TREES){
    tree <- tidytree::as_tibble(treeio::read.beast(file = paste0(treeFolderName, "\\", tree_name)))
    summary <- build_beast_tree_summary(tree, metadata_file)
    summaries[[tree_name]] <- summary
  }
  
  for(tree_name in TREETIME_TREES){
    tree <- tidytree::as_tibble(treeio::read.beast(file = paste0(treeFolderName,  "\\", tree_name)))
    confidence <- read.csv(paste0(treeFolderName, "\\", NEWICK_CONFIDENCE), header = TRUE)
    summary <- build_treetime_tree_summary(tree, confidence, metadata_file)
    summaries[[tree_name]] <- summary
  }
  
  return(summaries)
}

score_tree_by_bins <- function(tree_summary, nBins=NBINS){
  max_branch_length <- max(tree_summary$branch_lengths)
  bins <- 0:nBins/nBins*max_branch_length #splits branch length intervals in nBins equal segements
  split_tree <- list()
  for(i in 1:nBins){
    split_tree[[i]] <- list()
  }
  
  for(tree_element in tree_summary$tree_summary){
    bin <- which(tree_element$branch_length <= bins)[1] - 1 #-1 because first element of bins is 0 => bins shifted
    if(bin == 0){
      bin <- 1
    }
    split_tree[[bin]][[tree_element$name]] <- tree_element
  }
  
  scores <- list()
  
  for(i in 1:nBins){
    tree <- split_tree[[i]]
    
    accuracy_eur <- 0
    accuracy_che <- 0
    count_che <- 0
    count_eur <- 0
    
    for(sequence in tree){ ## should i not iterate through all the known sequences?
      
      if(sequence$ground_truth == "CHE"){
        if(sequence$inferred_type == sequence$ground_truth){
          accuracy_che <- accuracy_che + 1
        }
        count_che <- count_che + 1 #should i not divide by the total number of known sequences from the tsv file?
      } else {
        # print(paste(sequence$name, sequence$ground_truth))
        if(sequence$inferred_type == sequence$ground_truth){
          accuracy_eur <- accuracy_eur + 1
        }
        count_eur <- count_eur + 1
      }
      
    }
    
    accuracy_che <- accuracy_che / count_che
    accuracy_eur <- accuracy_eur / count_eur
    
    if(is.nan(accuracy_che)){
      accuracy_che <- -0.1
    }
    if(is.nan(accuracy_eur)){
      accuracy_eur <- -0.1
    }
    
    scores[[i]] <- list("left_bound" = bins[i], "right_bound" = bins[i+1], "accuracy_che" = accuracy_che, "accuracy_eur" = accuracy_eur, "count_che" = count_che, "count_eur" = count_eur)
  }
  
  return(scores)
}

plot_bar_accuracy <- function(scores){
  right_bounds <- c()
  acc_eur <- c()
  acc_che <- c()
  
  for(i in 1:length(scores)){
    right_bounds <- c(right_bounds, scores[[i]]$right_bound)
    acc_eur <- c(acc_eur, scores[[i]]$accuracy_eur)
    acc_che <- c(acc_che, scores[[i]]$accuracy_che)
  }
  
  a<-1
  df <- data.frame(accuracy = c(acc_eur, acc_che),
                   origin = c(rep("eur", length(acc_eur)), rep("che", length(acc_che))),
                   branch_length=right_bounds)
  
  p<-ggplot(data=df, aes(x=branch_length)) +
    geom_bar(aes(y=accuracy, fill = origin), stat="identity", position=position_dodge()) 
  
  return(p)
}

plot_box_accuracy <- function(scores){
  
  #FOR BDMM
  
  bins <- c()
  accuracy <- c()
  location <- c()
  boxplots <- list()
  method <- c()
  
  for(i in seq(1, length(scores), 2)){
    bdmm_summary <- scores[[i]]
    for(bin_number in 1:length(bdmm_summary)){
      
      #Add entry for the accuracy had for Europe
      if(bdmm_summary[[bin_number]]$count_eur > COUNT_THRESHOLD){
        bins <- c(bins, bin_number)
        accuracy <- c(accuracy, bdmm_summary[[bin_number]]$accuracy_eur)
        location <- c(location, "EUR")
        method <- c(method, "BDMM")
      }
      
      #Add entry for the accuracy had for Switzerland
      if(bdmm_summary[[bin_number]]$count_che > COUNT_THRESHOLD){
        bins <- c(bins, bin_number)
        accuracy <- c(accuracy, bdmm_summary[[bin_number]]$accuracy_che)
        location <- c(location, "CHE")
        method <- c(method, "BDMM")
      }
      
    }
      
  }
  
  bdmm_accuracies <- list("bins" = bins, "accuracy" = accuracy, "Location" = location, "Method" = method)
  bdmm_accuracies <- as.data.frame(bdmm_accuracies)
  bdmm_accuracies$bins <- as.factor(bdmm_accuracies$bins)
  
  boxplots[["bdmm"]] <- ggplot(bdmm_accuracies, aes(x=bins, y=accuracy, fill=Location)) + geom_boxplot()
  
  #FOR TREETIME
  
  bins <- c()
  accuracy <- c()
  location <- c()
  method <- c()
  
  for(i in seq(2, length(scores), 2)){
    treetime_summary <- scores[[i]]
    for(bin_number in 1:length(treetime_summary)){
      
      #Add entry for the accuracy had for Europe
      if(treetime_summary[[bin_number]]$count_eur > COUNT_THRESHOLD){
        bins <- c(bins, bin_number)
        accuracy <- c(accuracy, treetime_summary[[bin_number]]$accuracy_eur)
        location <- c(location, "EUR")
        method <- c(method, "TreeTime")
      }
      
      #Add entry for the accuracy had for Switzerland
      if(treetime_summary[[bin_number]]$count_che > COUNT_THRESHOLD){
        bins <- c(bins, bin_number)
        accuracy <- c(accuracy, treetime_summary[[bin_number]]$accuracy_che)
        location <- c(location, "CHE")
        method <- c(method, "TreeTime")
      }
      
    }
    
  }
  
  treetime_accuracies <- list("bins" = bins, "accuracy" = accuracy, "Location" = location, "Method" = method)
  treetime_accuracies <- as.data.frame(treetime_accuracies)
  treetime_accuracies$bins <- as.factor(treetime_accuracies$bins)
  
  boxplots[["treetime"]] <- ggplot(treetime_accuracies, aes(x=bins, y=accuracy, fill=Location)) + geom_boxplot()
  
  
  big_boxplot <- ggplot(rbind(bdmm_accuracies, treetime_accuracies), aes(x=bins, y=accuracy, fill=Location)) + 
    geom_boxplot() + 
    facet_grid(Method ~ ., scales = "free_y") +
    labs(y = "Accuracy", x = "Bin number")
  
  return(big_boxplot)
}


scores <- list()
plots <- list()

for(analysis_run in RUNS_FOLDER_NAMES){
  i <- 1
  trees_folder <- paste0(WORKDIR, "\\", analysis_run, "\\", TREE_FOLDER_NAME)
  metadata_file <- read.delim(file = paste0(WORKDIR, "\\", analysis_run, "\\", META_FOLDER_NAME, "\\", META_FILE), stringsAsFactors = F)
  summaries <- build_trees_summaries(trees_folder, metadata_file)
  
  for(tree_summary in summaries){
    analysis_name <- paste0(analysis_run, "_", names(summaries)[i])
    score <- score_tree_by_bins(tree_summary)
    scores[[analysis_name]] <- score
    p <- plot_bar_accuracy(score)
    plots[[analysis_name]] <- p
    i <- i+1
  }
}

sample_correction <- function(equil_freq_swiss){
  equil_freq_eur <- 1-equil_freq_swiss
  observed_freq_eur <- 0.25
  observed_freq_swiss <- 0.75
  
  ned <- 1-(equil_freq_swiss^2 + equil_freq_eur^2)
  dom <- 1-(observed_freq_swiss^2 + observed_freq_eur^2)
  
  return(ned/dom)
}


# boxes <- plot_box_accuracy(scores)

get_estimated_migrations_graph <- function(){
  treetime_base_che_to_eur <- 0.5061
  treetime_base_eur_to_che <- 41.6667
  
  treetime_substitution_rates <- c(1.663611, 2.04542, 1.630435, 1.888737)
  treetime_estim_che_to_eur <- treetime_substitution_rates * treetime_base_che_to_eur
  treetime_estim_eur_to_che <- treetime_substitution_rates * treetime_base_eur_to_che
  
  beast_estim_che_to_eur <- c(5.618, 3.818, 3.329, 3.534)
  beast_estim_eur_to_che <- c(81.82, 77.851, 64.816, 72.484)
  
  rates <- c()
  method <- c()
  location <- c()
  analysis_number <- c()
    
  for(i in 1:4){
    rates <- c(rates, treetime_estim_che_to_eur[i], treetime_estim_eur_to_che[i], beast_estim_che_to_eur[i], beast_estim_eur_to_che[i])
    method <- c(method, "Treetime", "Treetime", "BDMM", "BDMM")
    location <- c("CHE -> EUR","EUR -> CHE","CHE -> EUR", "EUR -> CHE")
    analysis_number <- c(analysis_number, i,i,i,i)
  }
  
  my_data <- list("rates"=rates, "Method"=method, "location"=location)
  my_data <- as.data.frame(my_data)
  
  plot <- ggplot(data = my_data, aes(analysis_number, rates, color=Method)) +
    labs(y = "Inferred migration count", x = "Data-set number") +
    geom_point() + 
    facet_grid(location ~ ., scales = "free_y")
  
  return(plot)
}

# plot<-get_estimated_migrations_graph()

get_tree_age_plot <- function(){
  
  treetime_root_age <- c(-0.999, -0.8765, -1.272, -1.104)
  beast_root_age <- c(-0.9717, -0.8985, -0.9091, -0.8538)
  
  ages <- c()
  method <- c()
  analysis_number <- c()
  
  for(i in 1:4){
    ages <- c(ages, treetime_root_age[i], beast_root_age[i])
    method <- c(method, "Treetime", "BDMM")
    analysis_number <- c(analysis_number, i,i)
  }
  
  my_data <- list("ages"=ages, "Method"=method)
  my_data <- as.data.frame(my_data)
  
  plot <- ggplot(data = my_data, aes(analysis_number, ages, color=Method)) +
    labs(y = "Inferred root age", x = "Data-set number") +
    geom_point()
  
  return(plot)
  
}

# plot <- get_tree_age_plot()
