const fs = require('fs-extra');
const _ = require('lodash');
const tsv = require('tsv');
const db = require('./database.js')

function getSequencesWithLocation(strExposureMappingFileLocation) {

    let fileContent = fs.readFileSync(strExposureMappingFileLocation, { encoding: 'utf-8' });
    let listExposureData = tsv.parse(fileContent);
    let mapExposureData = new Map();
    for (let objLocationData of listExposureData) {
        if (objLocationData.iso_country_exp != "XXX") {
            mapExposureData.set(objLocationData.gisaid_id, objLocationData.iso_country_exp);
        }
    }
    return mapExposureData;
}

function buildFastaEntry(objAvailableInformation, arrDesiredEntriesHeader, strSequenceColumn, strDateColumn, strLocationColumn) {
    strFastaHeader = ">";
    for (entry of arrDesiredEntriesHeader) {
        strFastaHeader = strFastaHeader + objAvailableInformation[entry] + "|";
    }

    if (objAvailableInformation[strLocationColumn] == "CHE") {
        strFastaHeader += "CHE|"
    } else {
        strFastaHeader += "EUR|"
    }

    strFastaHeader += objAvailableInformation[strDateColumn].toLocaleString();
    strFastaHeader = strFastaHeader.replace(/ /g, '');
    strFastaHeader += "\n";
    let strFastaEntry = strFastaHeader + objAvailableInformation[strSequenceColumn] + "\n";
    return strFastaEntry;
}

function buildCsvEntry(objAvailableInformation, arrDesiredEntriesHeader, strSequenceColumn, strDateColumn, strLocationColumn, strFastaName) {
    let strCsvEntry = strFastaName + ",";
    for (entry of arrDesiredEntriesHeader) {
        strCsvEntry = strCsvEntry + objAvailableInformation[entry] + ",";
    }

    if (objAvailableInformation[strLocationColumn] == "CHE") {
        strCsvEntry += "CHE,"
    } else {
        strCsvEntry += "EUR,"
    }

    strCsvEntry += objAvailableInformation[strDateColumn].toLocaleString();
    strCsvEntry += ",";
    strCsvEntry += objAvailableInformation[strSequenceColumn] + "\n";
    return strCsvEntry;
}

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
}

async function sampleSeqsFromIdArray(arrSeqIds, nSequenceNumber, strStartDate, strEndDate, strOutputFastaFilename, strOutputCsvFilename, strOutputSequenceIdOnlyFastaFilename, strAnnotationFilename = undefined, mapExposureData = undefined) {
    const nTotalEntries = arrSeqIds.length;

    let arrSequenceNumbers = [];
    while (arrSequenceNumbers.length < nSequenceNumber) {
        arrSequenceNumbers.push(getRandomIntInclusive(0, nTotalEntries - 1));
        arrSequenceNumbers = _.uniq(arrSequenceNumbers);
    }

    arrSequenceNumbers.sort(function (a, b) {
        return a - b;
    });
    let arrSequenceNumbersCopy = arrSequenceNumbers;
    arrSequenceNumbers.reverse();

    console.log(arrSequenceNumbers.length)
    let nNextEntry;
    let strNewFastaEntry = "";
    let strIdEntry = "";

    while (arrSequenceNumbers.length) {
        nNextEntry = arrSequenceNumbers.pop();
        strIdEntry = arrSeqIds[nNextEntry];
        objDbEntry = await db.whereKeyInArrayAndDateBetween("gisaid_api_sequence", "gisaid_epi_isl", [strIdEntry], "date_original", strStartDate, strEndDate)
        if (!objDbEntry.rows[0] || objDbEntry.rows[0].date_original.split("-").length != 3) {//if the date isn't complete, sample a new sequence id instead
            let old_length = arrSequenceNumbers.length;
            let new_length = arrSequenceNumbers.length;
            do { //to make sure we don t sample a number we already have in the set
                arrSequenceNumbers.push(getRandomIntInclusive(0, nTotalEntries - 1));
                arrSequenceNumbers = _.uniq(arrSequenceNumbers);
                new_length = arrSequenceNumbers.length;
            } while (new_length == old_length);
        } else {
            strNewFastaEntry = buildFastaEntry(objDbEntry.rows[0], ["gisaid_epi_isl", "strain", "virus"], "seq_aligned", "date_original", "country");
            strNewCsvEntry = buildCsvEntry(objDbEntry.rows[0], ["gisaid_epi_isl", "strain", "virus"], "seq_aligned", "date_original", "country", strNewFastaEntry.split("\n")[0].substring(1));
            
            console.log(arrSequenceNumbers.length)
            fs.appendFileSync(strOutputFastaFilename, strNewFastaEntry);
            fs.appendFileSync(strOutputCsvFilename, strNewCsvEntry);
            fs.appendFileSync(strOutputSequenceIdOnlyFastaFilename, `>${objDbEntry.rows[0].gisaid_epi_isl}\n${objDbEntry.rows[0].seq_aligned}\n` );
            if(strAnnotationFilename){
                fs.appendFileSync(strAnnotationFilename, `${strNewFastaEntry.split("\n")[0].substring(1)}\t${mapExposureData.get(objDbEntry.rows[0].gisaid_epi_isl)=="CHE" ? "CHE" : "EUR"}\n` );
            }
        }

    }
    return arrSequenceNumbersCopy;
}


async function getIdsByLocation(strKeyName, strLocation, strLocationColumnName){
    try{
        let arrIdsFromLocation = await db.getIdsByLocation("gisaid_api_sequence", strKeyName, strLocation, strLocationColumnName);
        let arrIds = [];
        for(objEntry of arrIdsFromLocation.rows){
            arrIds.push(objEntry.gisaid_epi_isl);
        }
        return arrIds;
    } catch(err){
        console.log(err)
    }
}

/**
 * 
 * @param {String} strExposureMappingFileLocation name of csv file that contains mapping of sequence id to exposure location
 * @param {Int16Array} nSwissKnownSeq number of swiss sequences with known exposure to be added to the dataset
 * @param {Int16Array} nSwissUnknownSeq number of swiss sequences with unknown exposure to be added to the dataset
 * @param {Int16Array} nEuropeKnownSeq number of sequences form the rest of Europe to be added to the dataset
 * @param {String} strStartDate the start date of the dataset (usually earliest date in strExposureMappingFileLocation)
 * @param {String} strEndDate the end date of the dataset (usually latest date in strExposureMappingFileLocation)
 * @param {String} strOutputFastaFilename name of output fasta file (for BEAST analysis, to be imported in BEAUTI)
 * @param {String} strOutputCsvFilename name of csv file used with treetime (as --states strOutputCsvFilename --attribute location)
 * @param {String} strOutputSequenceIdOnlyFastaFilename name of output fasta file (for generating initial Newick tree, with iqtree)
 * @param {String} strAnnotationFilename name of tsv to be imported into Figtree after creating the tree summary to annotate tips based on whether they are known/unknwon exposure location
 */
async function makeNewDataset(strExposureMappingFileLocation, nSwissKnownSeqFromSwiss, nSwissKnownSeqFromEur, nSwissUnknownSeq, nEuropeKnownSeq, strStartDate, strEndDate, strOutputFastaFilename, strOutputCsvFilename, strOutputSequenceIdOnlyFastaFilename, strAnnotationFilename) {
    try {
        await db.connect();
        //get ids of all known sequences
        fs.writeFileSync(strOutputFastaFilename, "");
        fs.writeFileSync(strOutputCsvFilename, "name, gisaid_epi_isl, strain, virus, location, date, seq_aligned\n");
        fs.writeFileSync(strOutputSequenceIdOnlyFastaFilename, "");
        fs.writeFileSync(strAnnotationFilename, "gisaid_epi_isl\tinfected_in_che_or_eur\n");

        const mapExposureData = getSequencesWithLocation(strExposureMappingFileLocation);

        const arrKnownSeqIds = Array.from(mapExposureData.keys())
        arrKnownSeqIds.pop(); // there's a trailing "" element for some reason

        let arrKnownSeqIdsFromEurope = [];
        let arrKnownSeqIdsFromSwiss = [];

        mapExposureData.forEach((location, seqId) => {
            if(location == "CHE"){
                arrKnownSeqIdsFromSwiss.push(seqId);
            } else {
                arrKnownSeqIdsFromEurope.push(seqId)
            }
        })

        console.log("Sampling known swiss sequences from Switz");
        await sampleSeqsFromIdArray(arrKnownSeqIdsFromSwiss, nSwissKnownSeqFromSwiss, strStartDate, strEndDate, strOutputFastaFilename, strOutputCsvFilename, strOutputSequenceIdOnlyFastaFilename, strAnnotationFilename, mapExposureData);
        
        console.log("Sampling known swiss sequences from Europe");
        await sampleSeqsFromIdArray(arrKnownSeqIdsFromEurope, nSwissKnownSeqFromEur, strStartDate, strEndDate, strOutputFastaFilename, strOutputCsvFilename, strOutputSequenceIdOnlyFastaFilename, strAnnotationFilename, mapExposureData);

        let arrSwissSequences = await getIdsByLocation("gisaid_epi_isl", "CHE", "country");
        // let arrSwissSequences = JSON.parse(fs.readFileSync("arrSwissSequences.JSON"));
        let arrUnknownSeqIds = _.difference(arrSwissSequences, arrKnownSeqIds)
        console.log("Sampling unknown swiss sequences");
        await sampleSeqsFromIdArray(arrUnknownSeqIds, nSwissUnknownSeq, strStartDate, strEndDate, strOutputFastaFilename, strOutputCsvFilename, strOutputSequenceIdOnlyFastaFilename);

        let arrEuropeanSequences = await getIdsByLocation("gisaid_epi_isl", "Europe", "region_original");
        // let arrEuropeanSequences = JSON.parse(fs.readFileSync("arrEuropeanSequences.JSON"));
        let arrEuropeMinusSwitz = _.difference(arrEuropeanSequences, arrSwissSequences);
        console.log("Sampling european sequences");
        await sampleSeqsFromIdArray(arrEuropeMinusSwitz, nEuropeKnownSeq, strStartDate, strEndDate, strOutputFastaFilename, strOutputCsvFilename, strOutputSequenceIdOnlyFastaFilename);

    } catch (error) {
        console.log(error)
    } finally {
        await db.disconnect();
    }
}



const strFolder = "./data/"
const strOutputFastaFilename = strFolder + "beast_analysis.fasta"
const strOutputCsvFilename = strFolder + "treetime_analysis.csv"
const strOutputSequenceIdOnlyFastaFilename = strFolder + "iqtree_generate.fasta"
const strAnnotationFilename = strFolder + "annotation_file.tsv"

makeNewDataset("./data/gisaid_exposure_mapping.tsv", 50, 50, 50, 50, "2020-04-01", "2020-11-04", strOutputFastaFilename, strOutputCsvFilename, strOutputSequenceIdOnlyFastaFilename, strAnnotationFilename);
